%global _empty_manifest_terminate_build 0
Name:           python-oslo-context
Version:        3.2.0
Release:        1
Summary:        Oslo Context library
License:        Apache-2.0
URL:            https://docs.openstack.org/oslo.context/latest/
Source0:        https://files.pythonhosted.org/packages/22/4e/336321171977ea62711f5d896e0d597346ca4f290e0caa2de1b10f748838/oslo.context-3.2.0.tar.gz
BuildArch:      noarch
%description
 Oslo Context Library The Oslo context library has helpers to maintain useful
information about a request context. The request context is usually populated in
the WSGI pipeline and used by various modules such as logging.

%package -n python3-oslo-context
Summary:        Oslo Context library
Provides:       python-oslo-context
# Base build requires
BuildRequires:  python3-devel
BuildRequires:  python3-setuptools
BuildRequires:  python3-pbr
BuildRequires:  python3-pip
BuildRequires:  python3-wheel
# General requires
BuildRequires:  python3-debtcollector
# Test requires
BuildRequires:  python3-pre-commit
BuildRequires:  python3-virtualenv
# General requires
Requires:       python3-debtcollector
Requires:       python3-pbr
%description -n python3-oslo-context
 Oslo Context Library The Oslo context library has helpers to maintain useful
information about a request context. The request context is usually populated in
the WSGI pipeline and used by various modules such as logging.

%package help
Summary:        Oslo Context library
Provides:       python3-oslo-context-doc
%description help
 Oslo Context Library The Oslo context library has helpers to maintain useful
information about a request context. The request context is usually populated in
the WSGI pipeline and used by various modules such as logging.

%prep
%autosetup -n oslo.context-3.2.0

%build
%py3_build


%install
%py3_install
install -d -m755 %{buildroot}/%{_pkgdocdir}
if [ -d doc ]; then cp -arf doc %{buildroot}/%{_pkgdocdir}; fi
if [ -d docs ]; then cp -arf docs %{buildroot}/%{_pkgdocdir}; fi
if [ -d example ]; then cp -arf example %{buildroot}/%{_pkgdocdir}; fi
if [ -d examples ]; then cp -arf examples %{buildroot}/%{_pkgdocdir}; fi
pushd %{buildroot}
if [ -d usr/lib ]; then
    find usr/lib -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/lib64 ]; then
    find usr/lib64 -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/bin ]; then
    find usr/bin -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/sbin ]; then
    find usr/sbin -type f -printf "/%h/%f\n" >> filelist.lst
fi
touch doclist.lst
if [ -d usr/share/man ]; then
    find usr/share/man -type f -printf "/%h/%f.gz\n" >> doclist.lst
fi
popd
mv %{buildroot}/filelist.lst .
mv %{buildroot}/doclist.lst .


%files -n python3-oslo-context -f filelist.lst
%dir %{python3_sitelib}/*


%files help -f doclist.lst
%{_docdir}/*

%changelog
* Mon Jul 12 2021 OpenStack_SIG <openstack@openeuler.org> - 3.2.0-1
- Upgrade version
* Fri Jan 29 2021 zhangy <zhangy1317@foxmail.com>
- Add buildrequires
* Fri Jan 08 2021 Python_Bot <Python_Bot@openeuler.org>
- Package Spec generated
